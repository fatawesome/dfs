FROM python:3.7

WORKDIR /app
COPY ./pyproject.toml ./
COPY ./poetry.lock ./
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -yq gcc python-dev
RUN pip3 install poetry==0.12
RUN poetry config settings.virtualenvs.create false
COPY . .
RUN poetry install --no-dev

EXPOSE 50051
