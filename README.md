# Distributed File System

The __Distributed File System__ (__DFS__) is a file system with data stored on a
server.
The data is accessed and processed as if it was stored on the local client
machine.
The DFS makes it convenient to share information and files among users on a
network.

## Client connection

1.  Clone the repository:

    ```
    $ git clone <repo url>
    ```

2.  Install all dependencies using
    [poetry](https://github.com/sdispater/poetry):

    ```
    $ poetry install
    ```

3.  Enter the virtualenv with all dependencies:

    ```
    $ poetry shell
    virtualenv$ ...
    ```

4.  Initialize the cluster:

    ```
    $ python3 -m dsdfs.client.init <server_address>
    ```

5.  Mount it somewhere:

    ```
    $ python3 -m dsdfs.client.fuse <server_address> /mnt/mountpoint
    ```

## Running the nodes

### Docker image

The default docker image used is `ionagamed/dsdfs:latest`, which is built on
every commit (hopefully, because there is no CI here).

### Docker swarm

Recommended deployment method to use this on a cluster with multiple physical
nodes is docker swarm (using the bundled compose file).

Assuming the node is already part of a swarm cluster:

```
$ docker stack deploy --compose-file docker-compose.yml dsdfs
$ docker service scale dsdfs_storage=2
```

__Note__: kubernetes is not supported, but can still work using
`--orchestrator kubernetes` argument for `docker stack` commands.

### Docker-compose

You can use docker-compose to run a single-node cluster.
As the default configuration requires at least 2 storage nodes to write anything
on it, you need to scale the `storage` service to at least 2 replicas (the
default configuration in `docker-compose.yml` supports up to 253 storage nodes
because of IP limitations).

```
$ docker-compose up -d --scale storage=3
```

### Manual - management

Management (or naming) server is the one who manages the directory structure,
keeps track of where files are stored and such stuff.

For now (TODO), this requires a running postgresql instance on `localhost:5432`,
with user `dsdfs` and database `dsdfs`.
All table creation will be done automatically.

1.  Clone the repository:

    ```
    $ git clone <repo url>
    ```

2.  Install all dependencies using
    [poetry](https://github.com/sdispater/poetry):

    ```
    $ poetry install
    ```

3.  Enter the virtualenv with all dependencies:

    ```
    $ poetry shell
    virtualenv$ ...
    ```

4.  Run!

    ```
    $ python3 -m dsdfs.management \
        --listen 0.0.0.0:50051 \
        --storage-nodes 10.0.8.0/24 \
        --create-tables
    ```

    Here, the specification of `--storage-nodes` is a CIDR subnet, where we will
    look up storage nodes using a simple heartbeat.

__Warning!__ - this does not use TLS for communication, so a VPN or Docker setup
is recommended for a production environment, or rather anywhere with
confidential data.

### Manual - storage

Storage node is a simple hashmap between a chunk id and actual storage.
You don't really need anything except the UNIX filesystem, which you probably
already have.

1.  Clone the repository:

    ```
    $ git clone <repo url>
    ```

2.  Install all dependencies using
    [poetry](https://github.com/sdispater/poetry):

    ```
    $ poetry install
    ```

3.  Enter the virtualenv with all dependencies:

    ```
    $ poetry shell
    virtualenv$ ...
    ```

4.  Run!

    ```
    $ python3 -m dsdfs.storage \
        --listen 0.0.0.0:50051 \
        -d /var/dsdfs-storage
    ```

    The directory creation will be automatically taken care of, but you should
    obviously have write access for it to occur.

## Configuration

Configuration is done through command line parameters.

### Management

```
$ python3 -m dsdfs.management --help
usage: __main__.py [-h] [--create-tables] --listen LISTEN
                   [--replicas REPLICAS] [--chunk-size CHUNK_SIZE]
                   --storage-nodes STORAGE_NODES

optional arguments:
  -h, --help            show this help message and exit
  --create-tables       Whether to try creating tables on startup
  --listen LISTEN, -l LISTEN
                        Listen address
  --replicas REPLICAS   Replica count (default 2)
  --chunk-size CHUNK_SIZE
                        Chunk size (default 16384, which is 16K)
  --storage-nodes STORAGE_NODES
                        Storage node subnet in CIDR format (x.y.z.w/N)
```

### Storage

```
$ python3 -m dsdfs.storage --help
usage: __main__.py [-h] --listen LISTEN --directory DIRECTORY

optional arguments:
  -h, --help            show this help message and exit
  --listen LISTEN, -l LISTEN
                        Listen address
  --directory DIRECTORY, -d DIRECTORY
                        Directory path to store chunks in
```
