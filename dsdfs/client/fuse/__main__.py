from argparse import ArgumentParser
import sys

from fuse import FUSE

from dsdfs.client.fuse.driver import Client


def parse_args():
    parser = ArgumentParser()
    parser.add_argument("server_addr")
    parser.add_argument("mountpoint")
    return parser.parse_args(sys.argv[1:])


def main():
    args = parse_args()
    FUSE(
        Client(args.server_addr),
        args.mountpoint,
        nothreads=True,
        foreground=True
    )


if __name__ == "__main__":
    main()
