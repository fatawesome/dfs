import datetime
import errno
import stat
import os
import grpc
from google.protobuf.empty_pb2 import Empty
from fuse import Operations, FuseOSError
from dsdfs.protocol import schema_pb2, schema_pb2_grpc


class Client(Operations):
    def __init__(self, server_addr):
        self.channel = grpc.insecure_channel(server_addr)
        self.stub = schema_pb2_grpc.ManagementServiceStub(self.channel)

    def chmod(self, *args, **kwargs):  # we don't support permissions
        return 0

    def readdir(self, path, fh):
        response = self.stub.GetDirectoryList(
            schema_pb2.GetDirectoryListRequest(
                path=path
            )
        )

        dirents = [".", ".."]

        for entry in response.entries:
            dirents.append(entry.name)

        for entry in dirents:
            yield entry

    def getattr(self, path, fh=None):
        try:
            response = self.stub.GetInfo(
                schema_pb2.GetInfoRequest(
                    path=path
                )
            )
        except Exception:
            raise FuseOSError(errno.ENOENT)

        attr = {
            "st_uid": os.getuid(),
            "st_gid": os.getgid(),
            "st_atime": datetime.datetime.now().timestamp(),
            "st_mtime": datetime.datetime.now().timestamp(),
        }

        if response.type == 0:
            attr["st_mode"] = stat.S_IFREG | int("644", 8)
            attr["st_nlink"] = 1
            attr["st_size"] = response.file_size
        else:
            attr["st_mode"] = stat.S_IFDIR | int("755", 8)
            attr["st_nlink"] = 2

        return attr

    def mkdir(self, path, mode):
        print(path)
        self.stub.CreateDirectory(
            schema_pb2.CreateDirectoryRequest(
                path=path
            )
        )
        return 0

    def rmdir(self, path):
        self.stub.DeleteDirectory(
            schema_pb2.DeleteDirectoryRequest(
                path=path
            )
        )
        return 0

    def read(self, path, length, offset, fh):
        response = self.stub.ReadFile(
            schema_pb2.ReadFileRequest(
                path=path
            )
        )

        data = response.contents[offset : offset + length]
        return data

    def create(self, path, mode):
        self.stub.WriteFile(
            schema_pb2.WriteFileRequest(
                path=path,
                contents=bytes()
            )
        )
        return 0

    def write(self, path, buf, offset, fh):
        contents = self.read(path, offset, 0, fh)

        self.stub.WriteFile(
            schema_pb2.WriteFileRequest(
                path=path,
                contents=contents + buf
            )
        )

        return len(buf)

    def truncate(self, path, length, fh=None):
        data = self.read(path, length, 0, fh)
        data = data.ljust(length, b"\x00")
        self.write(path, data, 0, fh)

    def unlink(self, path):
        self.stub.DeleteFile(
            schema_pb2.DeleteFileRequest(
                path=path
            )
        )
        return 0

    def rename(self, old, new):
        self.stub.MoveFile(
            schema_pb2.MoveFileRequest(
                source_path=old,
                dest_path=new
            )
        )
        return 0

    def statfs(self, path):
        statfs = self.stub.StatFS(Empty())
        magic = 4096  # zero ideas why is this working
        return {
            "f_frsize": magic,
            "f_blocks": statfs.bytes_total // magic,
            "f_bfree": statfs.bytes_free // magic,
            "f_bavail": statfs.bytes_free // magic,
        }
