from argparse import ArgumentParser
import sys

import grpc
from dsdfs.protocol import schema_pb2, schema_pb2_grpc


def parse_args():
    parser = ArgumentParser()
    parser.add_argument("server_addr")
    parser.add_argument("file_path")
    return parser.parse_args(sys.argv[1:])


def main():
    args = parse_args()

    channel = grpc.insecure_channel(args.server_addr)
    stub = schema_pb2_grpc.ManagementServiceStub(channel)

    response = stub.GetFileInfo(
        schema_pb2.GetFileInfoRequest(
            path=args.file_path
        )
    )

    total_len = len(str(len(response.chunks)))
    for i, chunk in enumerate(response.chunks):
        node_ids = [str(x) for x in chunk.node_ids]
        print(f"Chunk #{str(i + 1).zfill(total_len)}: {', '.join(node_ids)}")


if __name__ == "__main__":
    main()
