from argparse import ArgumentParser
import sys

import grpc
from google.protobuf.empty_pb2 import Empty
from dsdfs.protocol import schema_pb2_grpc


def parse_args():
    parser = ArgumentParser()
    parser.add_argument("server_addr")
    return parser.parse_args(sys.argv[1:])


def main():
    args = parse_args()
    channel = grpc.insecure_channel(args.server_addr)
    stub = schema_pb2_grpc.ManagementServiceStub(channel)
    response = stub.Init(Empty())
    print(f"Bytes available: {response.bytes_available}")
    print(f"AKA MB available: {response.bytes_available / 1024 / 1024}")
    print(f"AKA GB available: {response.bytes_available / 1024 / 1024 / 1024}")


if __name__ == "__main__":
    main()
