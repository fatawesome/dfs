from dsdfs.config.option import Option


def option(**kwargs):
    def outer_wrapper(fn):
        option = Option(fn, **kwargs)
        return option
    return outer_wrapper
