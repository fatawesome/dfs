class InvalidOptionValue(Exception):
    def __init__(self, option, message, orig):
        self.option = option
        self.message = message
        self.orig = orig

    def __str__(self):
        return (
            f"Invalid option value for option '{self.option.name}': '{self.message}' -"
            f" {self.orig}"
        )


class OptionRequired(Exception):
    def __init__(self, option):
        self.option = option

    def __str__(self):
        return f"Option '{self.option.name}' is required"
