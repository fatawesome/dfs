from dsdfs.config.exceptions import InvalidOptionValue, OptionRequired


class Option:
    def __init__(self, validator, required=False, default=None):
        self.name = validator.__name__
        self.validator = validator
        self.target_type = list(validator.__annotations__.values())[0]
        self.doc = validator.__doc__
        self.default = default

        self.required = required

    def coerce_type(self, value):
        if self.target_type == bool:
            return str(value).lower() in {"1", "y", "yes", "t", "true"}
        else:
            return self.target_type(value)

    def validate(self, value):
        if value is None and self.default:
            return self.coerce_type(self.default)
        if self.required:
            if value is None:
                raise OptionRequired(self)
            else:
                try:
                    return self.coerce_type(self.validator(value))
                except Exception as e:
                    raise InvalidOptionValue(self, value, e)
        else:
            if value is None:
                return None
            else:
                try:
                    return self.coerce_type(self.validator(value))
                except Exception as e:
                    raise InvalidOptionValue(self, value, e)
