import argparse
import sys
import os
import json

from dsdfs.config.exceptions import InvalidOptionValue


class CombinedArgumentParser:
    """
    Parses arguments from a multitude of sources.

    In order of precedence:
    *   Command line args
    *   Environment variables
    *   Configuration file (JSON)

    Parameter names are converted as (consider `parameter_name`):
    *   Command line arg: `--parameter-name` (notice how `_` changed to `-`)
    *   Environment variable: `IROHA_TUI_PARAMETER_NAME`
    *   Configuration file: `{"parameter_name": "value"}`
    """

    def __init__(self, options, env_namespace):
        self.cmdline_parser = argparse.ArgumentParser()
        self.options = options
        self._add_cmdline_args()

        self.option_map = {
            option.name: option for option in options
        }
        self.env_namespace = env_namespace.upper() + "_"

    def _add_cmdline_args(self):
        self.cmdline_parser.add_argument("--config", "-c", help="Config file path",
                                         required=False)
        for option in self.options:
            name = option.name.replace("_", "-")
            if option.target_type != bool:
                self.cmdline_parser.add_argument(f"--{name}", help=option.doc, required=False)
            else:
                self.cmdline_parser.add_argument(f"--{name}", help=option.doc, required=False,
                                                 action="store_true")

    def _set_config_value(self, config, key, value):
        try:
            config[key] = self.option_map[key].validate(value)
        except Exception as e:
            raise InvalidOptionValue(self.option_map[key], value, e)

    def _validate(self, config):
        validated_config = {}
        for key, value in config.items():
            self._set_config_value(validated_config, key, value)
        return validated_config

    def parse_args(self):
        config = {}

        args = vars(self.cmdline_parser.parse_args(sys.argv[1:]))
        config_path = args.pop("config", None)
        if config_path:
            with open(config_path, "r") as f:
                data = json.load(f)
            for key, value in data.items():
                config[key] = value

        for key, value in os.environ.items():
            if key.startswith(self.env_namespace):
                name = key[len(self.env_namespace):].lower()
                config[name] = value

        for key, value in args.items():
            if value or not config.get(key):  # override only if not None
                config[key] = value

        return self._validate(config)
