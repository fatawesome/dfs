from typing import Dict
from threading import Lock
import grpc


class GrpcChannelPool:
    def __init__(self):
        self.channels: Dict[str, grpc.Channel] = {}
        self.lock = Lock()

    def get_channel(self, address: str):
        with self.lock:
            if address not in self.channels:
                self.channels[address] = grpc.insecure_channel(address)
            return self.channels[address]
