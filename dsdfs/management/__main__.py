from concurrent import futures
from threading import Event

import grpc

from dsdfs.grpc_channel_pool import GrpcChannelPool
from dsdfs.protocol import schema_pb2_grpc

from dsdfs.management.models import register_session
from dsdfs.management.grpc import ManagementServiceServicer
from dsdfs.management.discovery import StorageNodeDiscovery
from dsdfs.management.storage import Storage
from dsdfs.management.config import load_config


def main():
    config = load_config()

    register_session(config["db_string"], config["create_tables"])
    channel_pool = GrpcChannelPool()
    storage = Storage(config["replicas"], config["chunk_size"], channel_pool)
    discovery = StorageNodeDiscovery(config["storage_nodes"], channel_pool, storage)
    executor = futures.ThreadPoolExecutor(max_workers=20)
    executor.submit(discovery.find_and_add_nodes_loop)

    server = grpc.server(executor)
    schema_pb2_grpc.add_ManagementServiceServicer_to_server(
        ManagementServiceServicer(storage, channel_pool), server
    )
    server.add_insecure_port(config["listen"])
    server.start()

    # cheap way to wait forever
    Event().wait()


if __name__ == "__main__":
    main()
