from dsdfs.config.parser import CombinedArgumentParser
from dsdfs.config.decorators import option


@option(default="postgres://dsdfs:dsdfs@localhost/dsdfs")
def db_string(value: str):
    return value


@option(default=False)
def create_tables(value: bool) -> bool:
    """
    Whether to create tables on startup.
    """

    return value


@option(required=True)
def listen(value: str) -> str:
    """
    Listen address
    """

    return value


@option(default=2)
def replicas(value: int) -> int:
    """
    Replica count (default 2).
    """

    return value


@option(default=16384)
def chunk_size(value: int) -> int:
    """
    Chunk size (default 16384, which is 16K)
    """

    return value


@option(required=True)
def storage_nodes(value: str) -> str:
    """
    Storage node subnet in CIDR format (x.y.z.w/N)
    """

    return value


def load_config():
    parser = CombinedArgumentParser([
        create_tables, listen, replicas, chunk_size, storage_nodes, db_string
    ], "dsdfs")
    args = parser.parse_args()
    return args
