from concurrent import futures
import ipaddress
from typing import List, Dict, TYPE_CHECKING
import time

from google.protobuf.empty_pb2 import Empty
from loguru import logger

from dsdfs.protocol import schema_pb2_grpc, schema_pb2
from dsdfs.management.models import Node, FileChunk


if TYPE_CHECKING:
    from dsdfs.grpc_channel_pool import GrpcChannelPool
    from dsdfs.management.storage import Storage


class StorageNodeDiscovery:
    """
    Performs storage node discovery on a CIDR block.
    """

    def __init__(
        self,
        cidr_block: str,
        channel_pool: "GrpcChannelPool",
        storage: "Storage",
        period: int = 5,
        preference: str = "Randomize"
    ):
        self.network: ipaddress.IPv4Network = ipaddress.ip_network(cidr_block)
        self.period = period
        self.channel_pool = channel_pool
        self.storage = storage
        self.executor = futures.ThreadPoolExecutor(max_workers=20)

    def run(self):
        self.executor.submit(self.find_and_add_nodes_loop)

    def find_and_add_nodes_loop(self):
        while True:
            time_begin = time.monotonic()
            try:
                self.find_and_add_nodes()
            except Exception as e:
                logger.exception(e)
            while time.monotonic() - time_begin < self.period:
                time.sleep(1)

    def find_and_add_nodes(self):
        existing_nodes: List[Node] = Node.all()
        ok_node_info: Dict[str, schema_pb2.GetStatusResponse] = {}
        futures_created = 0
        futures_finished = 0

        def make_future_done_callback(addr):
            def future_done_callback(v):
                # holy crap this is not good
                nonlocal futures_finished
                try:
                    ok_node_info[addr] = v.result()
                except Exception:
                    pass
                futures_finished += 1
            return future_done_callback

        # heartbeat to an IP range
        for host_addr in self.network.hosts():
            addr = f"{str(host_addr)}:50051"
            futures_created += 1
            self.send_heartbeat(addr).add_done_callback(
                make_future_done_callback(addr)
            )

        while futures_created > futures_finished:
            time.sleep(0.5)

        ok_existing_nodes: List[str] = []
        failing_nodes: List[str] = []

        # scanning through existing nodes, to find failing ones
        for node in existing_nodes:
            status = ok_node_info.pop(node.listen_addr, None)
            if not node.is_ok:
                continue  # this is already taken care of
            if status:
                node.bytes_free = status.bytes_free
                node.bytes_available = status.bytes_available
                node.save()
                ok_existing_nodes.append(node.listen_addr)
            else:
                node.is_ok = False
                node.save()
                self.executor.submit(self.reflow_chunks, node.id)
                failing_nodes.append(node.listen_addr)

        # scanning through the leftovers, they are actual new nodes
        for addr, status in ok_node_info.items():
            Node.create(listen_addr=addr, bytes_free=status.bytes_free,
                        bytes_available=status.bytes_available)

        if ok_node_info or failing_nodes:
            logger.info(
                ", ".join(
                    [
                        "Heartbeat complete, and something happened",
                        f"new nodes: {list(ok_node_info.keys())}",
                        f"failing nodes: {failing_nodes}",
                    ]
                )
            )

    def send_heartbeat(self, address: str):
        channel = self.channel_pool.get_channel(address)
        stub = schema_pb2_grpc.StorageServiceStub(channel)

        # we probably could not wait on all futures into an array, so this is a
        # kinda feasible option
        status_future = stub.GetStatus.future(Empty(), timeout=1)
        return status_future

    def reflow_chunks(self, node_id: int):
        from dsdfs.management.models.file_chunk import file_chunk_node

        related_chunks = (
            FileChunk.query
            .join(file_chunk_node)
            .filter(file_chunk_node.columns.node_id == node_id)
        )

        for chunk in related_chunks:
            try:
                self.reflow_chunk(node_id, chunk)
            except Exception as e:
                logger.exception(e)

        Node.where(id=node_id).first().delete()

    def reflow_chunk(self, node_id: int, chunk: FileChunk):
        chunk_contents = None
        not_here = set([node_id])

        # first we find something to get the contents from
        for node in chunk.nodes:
            not_here.add(node.id)
            try:
                chunk_contents = self.storage.raw.get_chunk(node.listen_addr, chunk.id)
                break
            except Exception:
                pass

        chunk.nodes.remove(Node.where(id=node_id).first())

        # then we store it somewhere
        for node in Node.get_sorted_by_load():
            if node.id not in not_here:
                try:
                    self.storage.raw.store_chunk(
                        node.listen_addr, chunk.id, chunk_contents
                    )
                    chunk.nodes.append(node)
                    break
                except Exception:
                    pass

        chunk.save()
