from typing import TYPE_CHECKING
from loguru import logger
from google.protobuf.empty_pb2 import Empty
from dsdfs.protocol import schema_pb2_grpc, schema_pb2
from dsdfs.management.path_utils import split_into_parts
from dsdfs.management.models import Directory, File, FileChunk


if TYPE_CHECKING:
    from dsdfs.grpc_channel_pool import GrpcChannelPool
    from dsdfs.management.storage import Storage


class ManagementServiceServicer(schema_pb2_grpc.ManagementServiceServicer):
    def __init__(self, storage: "Storage", channel_pool: "GrpcChannelPool"):
        self.channel_pool = channel_pool
        self.storage = storage

    def Init(self, request, context):
        self.storage.wipe()
        free, total = self.storage.get_stats()
        return schema_pb2.InitResponse(
            bytes_available=total
        )

    def ReadFile(self, request, context):
        try:
            file = File.get_by_path(request.path)
            contents = self.storage.get_file(file)
        except Exception as e:
            logger.exception(e)
        return schema_pb2.ReadFileResponse(
            contents=contents
        )

    def WriteFile(self, request, context):
        file = File.get_or_create_by_path(request.path)
        self.storage.store_file(file, request.contents)
        return Empty()

    def DeleteFile(self, request, context):
        file = File.get_by_path(request.path)
        self.storage.delete_file(file)
        return Empty()

    def GetFileInfo(self, request, context):
        file = File.get_by_path(request.path)

        response = schema_pb2.GetFileInfoResponse()
        response.size = file.size

        for chunk in FileChunk.where(file_id=file.id).all():
            chunk_data = schema_pb2.FileChunkInfo(id=chunk.id)
            for node in chunk.nodes:
                chunk_data.node_ids.append(node.id)
            response.chunks.append(chunk_data)

        return response

    def CopyFile(self, request, context):
        source_file = File.get_by_path(request.source_path)
        dest_file = File.get_or_create_by_path(request.dest_path)

        contents = self.storage.get_file(source_file)
        self.storage.store_file(dest_file, contents)
        return Empty()

    def MoveFile(self, request, context):
        file = File.get_by_path(request.source_path)
        parts = split_into_parts(request.dest_path)
        new_directory = Directory.get_by_path_parts(parts[:-1])
        file.directory_id = new_directory.id
        file.name = parts[-1]
        file.save()
        return Empty()

    def GetDirectoryList(self, request, context):
        directory = Directory.get_by_path(request.path)

        child_dirs = directory.get_direct_children()
        child_dir_names = sorted([x.name for x in child_dirs])

        child_files = File.where(directory_id=directory.id)
        child_file_names = sorted([x.name for x in child_files])

        response = schema_pb2.GetDirectoryListResponse()

        for name in child_dir_names:
            type_ = schema_pb2.GetDirectoryListResponse.Entry.Type.DIRECTORY
            response.entries.append(
                schema_pb2.GetDirectoryListResponse.Entry(type=type_, name=name)
            )

        for name in child_file_names:
            type_ = schema_pb2.GetDirectoryListResponse.Entry.Type.FILE
            response.entries.append(
                schema_pb2.GetDirectoryListResponse.Entry(type=type_, name=name)
            )

        return response

    def CreateDirectory(self, request, context):
        Directory.create_by_path(request.path)
        return Empty()

    def DeleteDirectory(self, request, context):
        directory = Directory.get_by_path(request.path)
        directory.delete_recursive()
        return Empty()

    def GetInfo(self, request, context):
        path = request.path

        try:
            Directory.get_by_path(path)
            return schema_pb2.GetInfoResponse(
                type=schema_pb2.GetInfoResponse.Type.DIRECTORY
            )
        except Exception:
            pass

        file = File.get_by_path(path)
        return schema_pb2.GetInfoResponse(
            type=schema_pb2.GetInfoResponse.Type.FILE,
            file_size=file.size
        )

    def StatFS(self, request, context):
        free, total = self.storage.get_stats()
        return schema_pb2.StatFSResponse(
            bytes_free=free,
            bytes_total=total
        )
