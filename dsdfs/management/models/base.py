from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Query, scoped_session
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_mixins import AllFeaturesMixin


Base = declarative_base()


def commit():
    try:
        Base._session.commit()
    except Exception as e:
        Base._session.rollback()
        raise e


class BaseModel(Base, AllFeaturesMixin):
    __abstract__ = True


def ensure_root_directory():
    """
    Ensure the existence of root directory.
    """

    from dsdfs.management.models import Directory

    roots = Directory.where(name="/").all()
    if not roots:
        Directory.create(id=0, name="/")
    elif len(roots) > 1:
        raise EnvironmentError("wtf")
    else:
        roots[0].id = 0
        roots[0].name = "/"
        roots[0].parent_id = None
        roots[0].save()


def register_session(db_string, create_tables=False):
    engine = create_engine(db_string)
    session_factory = sessionmaker(bind=engine, autocommit=True, autoflush=False)
    session = scoped_session(session_factory)
    if create_tables:
        Base.metadata.create_all(engine)
    BaseModel.set_session(session)
    ensure_root_directory()
