from typing import List
from sqlalchemy import Column, Integer, String, ForeignKey, UniqueConstraint
from dsdfs.management.path_utils import split_into_parts
from dsdfs.management.models.base import BaseModel


class Directory(BaseModel):
    __tablename__ = "directory"
    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    parent_id = Column(Integer, ForeignKey("directory.id", ondelete="CASCADE"))

    __table_args__ = (UniqueConstraint("name", "parent_id", name="_name_parent_uc"),)

    def get_direct_children(self) -> List["Directory"]:
        return Directory.where(parent_id=self.id)

    def delete_recursive(self):
        for child in self.get_direct_children():
            child.delete_recursive()
        self.delete()

    @classmethod
    def get_by_path(cls, path: str) -> "Directory":
        return cls.get_by_path_parts(split_into_parts(path))

    @classmethod
    def get_parent_by_path(cls, path: str) -> "Directory":
        return cls.get_by_path_parts(split_into_parts(path)[:-1])

    @classmethod
    def create_by_path(cls, path: str):
        parts = split_into_parts(path)
        parent = cls.get_by_path_parts(parts[:-1])
        directory = Directory(name=parts[-1], parent_id=parent.id)
        directory.save()

    @classmethod
    def get_by_path_parts(cls, parts: List[str]) -> "Directory":
        directory = cls._get_root()
        for part in parts:
            directory = (
                cls.query
                .filter(
                    (Directory.parent_id == directory.id) & (Directory.name == part)
                )
                .first()
            )
            if not directory:
                raise ValueError("Path not found")
        return directory

    @classmethod
    def _get_root(cls) -> "Directory":
        return cls.where(name="/").first()
