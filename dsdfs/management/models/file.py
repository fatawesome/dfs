from sqlalchemy import Column, Integer, String, ForeignKey, UniqueConstraint, BigInteger
from dsdfs.management.path_utils import split_into_parts
from dsdfs.management.models.base import BaseModel


class File(BaseModel):
    __tablename__ = "file"

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    size = Column(BigInteger)
    directory_id = Column(Integer, ForeignKey("directory.id"))

    __table_args__ = (
        UniqueConstraint("name", "directory_id", name="_name_directory_uc"),
    )

    @classmethod
    def get_or_create_by_path(cls, path: str) -> "File":
        from dsdfs.management.models import Directory

        parts = split_into_parts(path)
        directory = Directory.get_by_path_parts(parts[:-1])

        file = (
            File.query
            .filter((File.directory_id == directory.id) & (File.name == parts[-1]))
            .first()
        )

        if not file:
            file = File(name=parts[-1], directory_id=directory.id).save()

        return file

    @classmethod
    def get_by_path(cls, path: str) -> "File":
        from dsdfs.management.models import Directory

        parts = split_into_parts(path)
        directory = Directory.get_by_path_parts(parts[:-1])

        file = (
            File.query
            .filter((File.directory_id == directory.id) & (File.name == parts[-1]))
            .first()
        )

        return file

    @classmethod
    def create_by_path(cls, path: str) -> "File":
        from dsdfs.management.models import Directory

        parts = split_into_parts(path)
        directory = Directory.get_by_path_parts(parts[:-1])

        file = File.create(name=parts[-1], directory_id=directory.id)

        return file
