from sqlalchemy import Column, Integer, ForeignKey, UniqueConstraint, Table
from sqlalchemy.orm import relationship
from dsdfs.management.models.base import BaseModel


file_chunk_node = Table(
    "file_chunk__node",
    BaseModel.metadata,
    Column(
        "file_chunk_id",
        Integer,
        ForeignKey("file_chunk.id", ondelete="CASCADE"),
        nullable=False,
    ),
    Column("node_id", Integer, ForeignKey("node.id"), nullable=False),
)


class FileChunk(BaseModel):
    __tablename__ = "file_chunk"

    id = Column(Integer, primary_key=True)
    file_id = Column(Integer, ForeignKey("file.id"), nullable=False)
    order = Column(Integer, nullable=True)
    nodes = relationship(
        "Node",
        secondary=file_chunk_node,
    )

    __table_args__ = (UniqueConstraint("file_id", "order", name="_file_order_uc"),)
