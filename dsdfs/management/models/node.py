from typing import List
from sqlalchemy import Column, Integer, String, Boolean, BigInteger, text
from dsdfs.management.models.base import BaseModel


class Node(BaseModel):
    __tablename__ = "node"

    id = Column(Integer, primary_key=True)
    listen_addr = Column(String(255), nullable=False, unique=True)
    bytes_available = Column(BigInteger, nullable=False)
    bytes_free = Column(BigInteger, nullable=False)
    is_ok = Column(Boolean, default=True, nullable=False)

    @classmethod
    def get_sorted_by_load(cls) -> List["Node"]:
        return cls.query.order_by(cls.bytes_free.desc(), text("RANDOM()"))
