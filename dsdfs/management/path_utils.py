from typing import List


def split_into_parts(path: str) -> List[str]:
    return [x for x in path.split("/") if x]
