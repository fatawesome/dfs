from typing import TYPE_CHECKING, List, Tuple
from loguru import logger
from google.protobuf.empty_pb2 import Empty
from dsdfs.protocol import schema_pb2, schema_pb2_grpc
from dsdfs.management.models import FileChunk, Node, Directory, File


if TYPE_CHECKING:
    from dsdfs.grpc_channel_pool import GrpcChannelPool


class RawStorage:
    def __init__(self, channel_pool: "GrpcChannelPool"):
        self.channel_pool = channel_pool

    def wipe(self, address: str):
        self._create_stub(address).Wipe(Empty())

    def store_chunk(self, address: str, chunk_id: int, contents: bytes):
        self._create_stub(address).StoreChunk(
            schema_pb2.StoreChunkRequest(
                chunk=schema_pb2.FileChunk(id=chunk_id, contents=contents)
            )
        )

    def get_chunk(self, address: str, chunk_id: int) -> bytes:
        response = self._create_stub(address).GetChunk(
            schema_pb2.GetChunkRequest(id=chunk_id)
        )
        return response.contents

    def delete_chunk(self, address: str, chunk_id: int):
        self._create_stub(address).DeleteChunk(
            schema_pb2.DeleteChunkRequest(id=chunk_id)
        )

    def _create_stub(self, address: str):
        channel = self.channel_pool.get_channel(address)
        stub = schema_pb2_grpc.StorageServiceStub(channel)
        return stub


class Storage:
    def __init__(self, replicas: int, chunk_size: int, channel_pool: "GrpcChannelPool"):
        self.raw = RawStorage(channel_pool)
        self.replicas = replicas
        self.chunk_size = chunk_size
        self.channel_pool = channel_pool

    def get_stats(self):
        free = sum(x.bytes_free for x in Node.all()) // self.replicas
        total = sum(x.bytes_available for x in Node.all()) // self.replicas
        return free, total

    def wipe(self):
        from dsdfs.management.models.base import ensure_root_directory
        for file_chunk in FileChunk.all():
            file_chunk.delete()
        for file in File.all():
            file.delete()
        Directory._get_root().delete()  # cascades will drop everything
        ensure_root_directory()
        for node in Node.all():
            try:
                self.raw.wipe(node.listen_addr)
            except Exception:
                pass

    def store_file(self, file: "File", contents: bytes):
        file.size = len(contents)
        file.save()

        chunks, data_chunks = self._create_chunks(file, contents)
        for chunk, data in zip(chunks, data_chunks):
            success_nodes = []
            for node in Node.get_sorted_by_load():
                try:
                    self.raw.store_chunk(node.listen_addr, chunk.id, data)
                    success_nodes.append(node)
                    chunk.nodes.append(node)
                    if len(success_nodes) == self.replicas:
                        break
                except Exception as e:
                    logger.exception(e)
            if len(success_nodes) != self.replicas:
                raise ValueError("Not enough nodes to satisfy the operation")
            chunk.save()

    def get_file(self, file: "File") -> bytes:
        chunks = (
            FileChunk.where(file_id=file.id).order_by(FileChunk.order)
        )

        data_chunks = []

        for chunk in chunks:
            for node in chunk.nodes:
                try:
                    data = self.raw.get_chunk(node.listen_addr, chunk.id)
                    data_chunks.append(data)
                    break
                except Exception:
                    pass

        return b"".join(data_chunks)

    def delete_file(self, file: "File"):
        for chunk in FileChunk.where(file_id=file.id).all():
            for node in chunk.nodes:
                self.raw.delete_chunk(node.listen_addr, chunk.id)
            chunk.delete()
        file.delete()

    def _create_chunks(
        self, file: "File", contents: bytes
    ) -> Tuple[List[FileChunk], List[bytes]]:
        FileChunk.where(file_id=file.id).delete()
        data_chunks = self._split_into_chunk_data(contents)
        chunks = []
        for i, data in enumerate(data_chunks):
            chunks.append(FileChunk(file_id=file.id, order=i).save())
        return chunks, data_chunks

    def _split_into_chunk_data(self, contents: bytes) -> List[bytes]:
        chunks = []
        for i in range(0, len(contents), self.chunk_size):
            chunks.append(contents[i : i + self.chunk_size])
        return chunks
