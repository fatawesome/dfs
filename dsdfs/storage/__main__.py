from concurrent import futures
from threading import Event
import grpc
from dsdfs.protocol import schema_pb2_grpc
from dsdfs.storage.driver import FilesystemDriver
from dsdfs.storage.grpc import StorageServiceServicer
from dsdfs.storage.config import load_config


def main():
    config = load_config()

    driver = FilesystemDriver(config["directory"])

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    schema_pb2_grpc.add_StorageServiceServicer_to_server(
        StorageServiceServicer(driver), server
    )
    server.add_insecure_port(config["listen"])
    server.start()

    # cheap way to wait forever
    Event().wait()


if __name__ == "__main__":
    main()
