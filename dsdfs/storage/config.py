from dsdfs.config.parser import CombinedArgumentParser
from dsdfs.config.decorators import option


@option(required=True)
def listen(value: str):
    """
    Listen address
    """

    return value


@option(required=True)
def directory(value: str):
    """
    Directory path to store chunks in
    """

    return value


def load_config():
    parser = CombinedArgumentParser([listen, directory], "dsdfs")
    return parser.parse_args()
