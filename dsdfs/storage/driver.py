from typing import Tuple
import shutil
import os


class FilesystemDriver:
    def __init__(self, base_path):
        self.base_path = base_path
        os.makedirs(self.base_path, exist_ok=True)

    def wipe(self):
        shutil.rmtree(self.base_path)
        os.makedirs(self.base_path, exist_ok=True)

    def write_chunk(self, chunk_id, contents):
        with open(self._get_path(chunk_id), "wb") as f:
            f.write(contents)

    def read_chunk(self, chunk_id) -> bytes:
        with open(self._get_path(chunk_id), "rb") as f:
            data = f.read()
        return data

    def get_status(self) -> Tuple[int, int]:
        stat = os.statvfs(self.base_path)
        bytes_free = stat.f_frsize * stat.f_bavail
        bytes_total = stat.f_frsize * stat.f_blocks
        return bytes_free, bytes_total

    def delete_chunk(self, chunk_id):
        os.remove(self._get_path(chunk_id))

    def _get_path(self, chunk_id):
        return os.path.join(self.base_path, str(chunk_id))
