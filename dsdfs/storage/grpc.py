from typing import TYPE_CHECKING
from google.protobuf.empty_pb2 import Empty
from dsdfs.protocol import schema_pb2_grpc, schema_pb2

if TYPE_CHECKING:
    from dsdfs.storage.driver import FilesystemDriver


class StorageServiceServicer(schema_pb2_grpc.StorageServiceServicer):
    def __init__(self, driver: "FilesystemDriver"):
        self.driver = driver

    def Wipe(self, request, context):
        self.driver.wipe()
        bytes_free, bytes_available = self.driver.get_status()
        return schema_pb2.WipeResponse(
            bytes_available=bytes_available
        )

    def StoreChunk(self, request, context):
        self.driver.write_chunk(request.chunk.id, request.chunk.contents)
        return Empty()

    def GetChunk(self, request, context):
        contents = self.driver.read_chunk(request.id)
        return schema_pb2.GetChunkResponse(contents=contents)

    def DeleteChunk(self, request, context):
        self.driver.delete_chunk(request.id)
        return Empty()

    def GetStatus(self, request, context):
        bytes_free, bytes_total = self.driver.get_status()
        return schema_pb2.GetStatusResponse(bytes_free=bytes_free,
                                            bytes_available=bytes_total)
