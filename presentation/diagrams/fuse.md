<style> #page-header {display:none;} .markdown-body {border:none;} </style>

```mermaid
graph LR;
    U1[User1];
    F1[Fuse driver];
    U2[User2];
    F2[Fuse driver];
    MS[Management server];

    subgraph Box 1
        U1 -->|simple posix| F1;
    end
    subgraph Box 2
        U2 -->|simple posix| F2;
    end

    F1 -->|grpc| MS;
    F2 -->|grpc| MS;
```
