<style> #page-header {display:none;} .markdown-body {border:none;} </style>

```mermaid
graph LR;
    C1[Client 1]
    C2[Client 2]
    C3[Client 3]
    MS[Management server]
    MSD[PostgreSQL database]
    S1[Storage server 1]
    S2[Storage server 2]
    S3[Storage server 3]

    C1 -->|grpc| MS;
    C2 -->|grpc| MS;
    C3 -->|grpc| MS;

    MS -->|psql| MSD;

    MS -->|grpc| S1;
    MS -->|grpc| S2;
    MS -->|grpc| S3;
```

```mermaid
graph LR;
    C[Client];
    MS[Management server];
    MSD[PostgreSQL database];
    S1[Storage server 1];
    S2[Storage server 2];
    S3[Storage server 3];

    C -->|store this file pls: /dir/file| MS;
    MS -->|file with chunks 5, 7, 16 is in 'dir' and named 'file'| MSD;
    MS -->|store chunks 5, 7| S1;
    MS -->|store chunks 7, 16| S2;
    MS -->|store chunks 5, 16| S3;
```

```mermaid
sequenceDiagram;
    participant C as Client
    participant MS as Management
    participant S1 as Storage 1
    participant S2 as Storage 2
    participant S3 as Storage 3

    C ->> MS: Store this
    MS ->> S1: Store chunk
    S1 ->> MS: Ok
    MS ->> S2: Store chunk
    S2 ->> MS: Ok
    MS ->> S3: Store chunk
    S3 ->> MS: Ok
    MS ->> C: Ok
```

```mermaid
sequenceDiagram;
    participant C as Client
    participant MS as Management
    participant S1 as Storage 1
    participant S2 as Storage 2
    participant S3 as Storage 3

    C ->> MS: Store this
    MS ->> S1: Store chunk
    MS ->> S2: Store chunk
    MS ->> S3: Store chunk
    S2 ->> MS: Ok
    S3 ->> MS: Ok
    S1 ->> MS: Ok
    MS ->> C: ok
```

```mermaid
sequenceDiagram
    participant MS as Management
    participant S1 as Storage 1
    participant S2 as Storage 2
    participant S3 as Storage 3

    MS -->> S1: failed heartbeat
    MS ->> S2: Gimme chunk 1
    MS ->> S3: Store chunk 1
    MS ->> S3: Gimme chunk 2
    MS ->> S2: Store chunk 2
    Note over MS: S1 is dead to me
```

