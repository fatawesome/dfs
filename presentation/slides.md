# DSDFS - DS Distributed File System

---

# Agenda

*   Overall architecture
*   Fault-tolerance
*   FUSE
*   Obvious problems
*   Live-demo

---

# Overall architecture

![](./images/lowlevel_overall.png)

---

# Technologies chosen

*   Python - fast prototyping
*   gRPC & Protobuf - simple, fast
*   PostgreSQL - we have experience

---

class: widepic

![](./images/highlevel_overall.png)

---

class: widepic

![](./images/slow_sync.png)

---

class: widepic

![](./images/fast_async.png)

---

# Fault-tolerance

*   Each file chunk is stored on some number of nodes
*   This is an invariant even when a node fails

---

class: widepic

![](./images/fault_tolerance.png)

---

# Client interface

*   Main driver - FUSE
*   Because it allows a UNIX-familiar user to use it as any other FS

---

class: widepic

![](./images/fuse.png)

---

# Additional

*   Of course, FUSE doesn't give us everything that we want
*   There is an additional utility entrypoint for `Init` call, for example

---

# Obvious problems

*   We don't have a 2-phase commit, or anything like that, so we don't handle
    mid-write fails well

---

# Now for the live demo
