#!/bin/bash

python3 -m dsdfs.management \
    --listen 127.0.0.1:50051 \
    --storage-nodes 127.0.0.0/24 \
    --create-tables
