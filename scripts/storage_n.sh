#!/bin/bash

python3 -m dsdfs.storage \
    --listen 127.0.0.10$1:50051 \
    --directory /tmp/dsdfs$1
